from setuptools import setup, find_packages

setup(
    name='powerline_date',
    version='0.0.2',
    author='Colin Wood',
    author_email='cwood06@gmail.com',
    install_requires=[
        'python-dateutil',
    ],
    url='https://bitbucket.org/colinbits/powerline-date',
    download_url='https://bitbucket.org/colinbits/powerline-date',
    long_description=open('README').read(),
    include_package_data=True,
    packages=find_packages(),
    py_modules=['powerline_date'],
    description='A powerline date segment replacement',
    tests_require=[
        'nose',
    ],
    keywords=[],
    package_data={
        '': ['README'],
    },
)
